﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class ContinueButton : MonoBehaviour {

	//TxtMsg is the object of the Text in Panel and HB is the HealthBar Object so I can turn it on and off
	public GameObject txtMsg;
	public GameObject HB;
	//I create a list of Animation clips so that I can play them easily
	public List<AnimationClip> clips = new List<AnimationClip>();
	//_currClip is a counter used for the list of clips and counter is used so that I can play the new animation every 2 clicks
	public int _currClip;
	public int counter;
	//Continue function runs every time the countinue button is clicked
	public void Continue()
	{
		//I am getting the text objects compent so that I can read a line from the text file
		txtMsg.GetComponent<TextReader> ().ReadFromFile ();
		//Simple if statement so that I will only play an animation every 2 clicks
		if (_currClip != clips.Count && counter % 2 == 0) {

			Camera.main.GetComponent<Animation> ().PlayQueued (clips [_currClip].name);
			_currClip++;
		}
		//An if statement to show the heathbar on the 16th click
		if (counter == 16 || counter == 17) {
			HB.SetActive (true);
		} else
			HB.SetActive (false);
		if (counter == 25)
			Application.LoadLevel (2);
		
		counter++;


	}
}
