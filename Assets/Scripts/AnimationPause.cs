﻿using UnityEngine;
using System.Collections;

public class AnimationPause : MonoBehaviour {

	// Use this for initialization
	void Start () {
		this.GetComponent<Animator> ().StopPlayback ();
	
	}

}
